# ipBlock.rs

An Engine to manage IPs blocked in security technologies.

# Intro
The idea of this project is to create an Engine that will handle blocks and unblocks of IP Addresses being dynamic enough to work on *any* given block end (e.g website, CDN, firewall) given that this configuration can be set remotelly from any host.
PS: This project will be made in **Rust**.

## Function Details

#### **Block_IP** 
Will receive a set of command and variables that will be used to block the IP in the ending program. The idea is that here a custom command will be executed with a set of variables that will be acquired from the program. e.g.: The block works on a tool that has a web API for blocking, and in this scenario, you'll set a curl command with the necessary information. 
Extra e.g: `curl <proto>://<block_api_host>/<block_endpoint>/{ip_to_block}` . The idea of expanding variables could also work on body, headers and so on, fully customizable.

If the IP being asked to be blocked is already blocked, add more time to this block. This here can also have an escalating formula, e.g: 1st block - 24hr, second block while still block - 36hr and so on.

The HTTP(s) endpoint should probably be implemented from inside rust itself, since it's a common scenario. Other ones that may use more custom tools could enable a "bash"-like command to be executed.

#### **Unblock_IP** 
Quite the same as [[#Block_IP]] in pretty much every aspect, but the difference is that this endpoint will be used to remove the block from the IP. Again, time-based things will be handled and stored in the database.

#### **Check_Blocks** 
This function will be executed from time to time (e.g each hour) to check if the time to be blocked until for every IP isn't higher than the actual time, basically checks if the current block is expired or not, and if so, remove the block with the Unblock_IP function.

### Flowchart - execution of the final product

```mermaid
flowchart TB
id1[IP Asked to block via API]
id2[Check_Blocks routine]
id3[Block_IP]
id4[Unblock_IP]
id5{Block Expired?}
id6[Wait given time]
id7[IP Asked to unblock via API]
db[(Database)]

id1-->id3-->db
id2-->id5-- Yes -->id4
id5-- No -->id6-->id5
id7-->id4
id4-->db
```

## Database Schema

#### ipInfo Table
| Constraint | Field        | Type    | Can be Null | Comment                                                        |
| ---------- | ------------ | ------- | ----------- | -------------------------------------------------------------- |
| PK         | ipAddress    | String  | No          | IP Address.                                                    |
| -          | isBlocked    | Boolean | No          | If the IP is blocked.                                          |
| -          | blockedUntil | Date    | Yes         | Until when the IP is blocked.                                  |
| -          | blockForEver | Boolean | No          | If the IP should be kept blocked forever.                      |
| -          | firstSeen    | Date    | No          | The first time the IP interacted with the block engine.        |
| -          | lastSeen     | Date    | No          | The most recent event the IP interacted with the block engine. | 
|            |              |         |             |                                                                |


#### ipBlocks Table
| Constraint | Field          | Type     | Can be Null | Comment                                                                                   |
| ---------- | -------------- | -------- | ----------- | ----------------------------------------------------------------------------------------- |
| PK         | blockId        | UUID/str | No          | Unique ID for the block.                                                                  |
| FK         | ipAddress      | String   | No          | The IP Address being blocked.                                                             |
| -          | blockTime      | Date     | No          | When the block happened.                                                                  |
| -          | blockFor       | Date     | No          | Ammount of time the IP was blocked for. Has a default time.                               |
| -          | blockComment   | String   | No          | Comment given with the block in question.                                                 |
| -          | blockSource    | String   | No          | Source that asked for the block. Can be a rule or tool name. Defaults to manual if empty. | 
| -          | blockRequester | String   | No          | Who asked for the block to be made. Defaults to blockSource if empty.                     |

---
#rust #project #open_source